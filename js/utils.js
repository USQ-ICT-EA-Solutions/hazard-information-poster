//Available tabs
var tabs = new Array("tab-1", "tab-2", "tab-3", "tab-4");
var hideTabs; 

function change(oldClass, newClass) 
{
   var tagged=document.getElementsByTagName('a');
   for(var i = 0 ; i < tagged.length ; i++)
   {
      if (tagged[i].className==oldClass)
      {
         tagged[i].className=newClass;
      }
   }
}

function displayTab(tab)
{
   for (var i=0; i < 4; i++)
   {
     if (document.getElementById("group" + (i+1))) {
      document.getElementById("group" + (i+1)).style.display = "none";
     }
   }
   
   if (tab != -1)
   {
      document.getElementById("group" + (tab+1)).style.display = "block";
   }
}

function switchTab(tabNum)
{
   for (var i=0; i<tabs.length; i++)
   {
      //We just want to apply the CSS on start-up
      //Clear all the Ids if any exists      
      if (document.getElementById("tablink-" + (i+1)))
         document.getElementById("tablink-" + (i+1)).id = "";
         
      if (document.getElementById("group-" + (i+1)))
         document.getElementById("group-" + (i+1)).id = "";
         
      change(tabs[i] + "on", tabs[i]);       
   }
   
   if (tabNum != -1)
   {
      change(tabs[tabNum], tabs[tabNum] + "on");       
   }
   
   displayTab(tabNum);
   
   //Clear any timeout of hiding tabs
   clearTimeout(hideTabs);
}

//Hide all tabs and link groups
function hideAllTabs()
{
   hideTabs = setTimeout("switchTab(-1)",10000);
}

//add style to odd rows in tables with class atable

$(document).ready(function($) {
  //for table row
  $(".atable tr:odd").addClass("row-colored");
});