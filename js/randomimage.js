var alreadyRun = false;
function randomimage() {
   if (alreadyRun) { return; }
   alreadyRun = true;

        // Select a random image from the list below for the page.

	var theImg = new Array(); 
        
        // The list of potential images
	theImg[0] = 'http://www.safetyandwellbeing.uts.edu.au/images/css/splash-main-graphic1.jpg';
	theImg[1] = 'http://www.safetyandwellbeing.uts.edu.au/images/css/splash-main-graphic2.jpg';
	theImg[2] = 'http://www.safetyandwellbeing.uts.edu.au/images/css/splash-main-graphic3.jpg';
	theImg[3] = 'http://www.safetyandwellbeing.uts.edu.au/images/css/splash-main-graphic4.jpg';
	theImg[4] = 'http://www.safetyandwellbeing.uts.edu.au/images/css/splash-main-graphic5.jpg';

       // Select an image from the array and put it as the background of the centre t-shape
        var p = theImg.length;
	var whichImage = Math.round(Math.random()*(p-1));
	document.getElementById('banner-wrapper').style.backgroundImage="url("+theImg[whichImage]+")";
}