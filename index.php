<?php
date_default_timezone_set( "Australia/Brisbane" );

$stickers = array();

function loadStickers() {
  global $stickers;
  $file_handle = fopen( "stickers.csv", "r" );
  while ( !feof( $file_handle ) ) {
    $line       = fgetcsv( $file_handle, 1024 );
    $stickers[] = $line;
  }

  fclose( $file_handle );
  return;
}

function getStickers( $category, $cssClass ) {
  global $stickers;
  foreach ( $stickers as $sticker ) {

    $cat = $sticker[ 2 ];

    if ( $cat == $category ) {
		$text = $sticker[ 0 ];
		$key  = $sticker[ 1 ];
		$id = $category . '_' . $key;

		echo '<div class="form-group checkbox ' . $cssClass . ' " >';
		echo '	<input id="' . $id . '" type="checkbox" name="' . $cat . 's[]" value="' . $key . '" >';
		echo '	<label class="checkbox-inline" for="' . $id . '">';
		echo '		<img src="img/stickers/' . $cat . '/' . $key . '.png" class="img-thumbnail thumbnail" />';
		echo '		' . $text . '';
		echo '	</label>';
		echo '</div>';
    }
  }
}

loadStickers();
?>
<!DOCTYPE html>

<html>

  <head>
    <!-- Meta -->
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <title>USQ Hazard Information Poster Generator</title>

    <!-- CSS Libraries -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

	<!-- USQ Marketing Template styles -->
    <link href="/Content/USQ/Responsive/Css/master.min.css" rel="stylesheet" type="text/css" />
	
	<!-- http://flatlogic.github.io/awesome-bootstrap-checkbox -->
	<link href="css/build.css" rel="stylesheet" type="text/css" />
	
	<!-- USQ Application-specific style -->
    <link href="css/usq.css" rel="stylesheet" type="text/css" />

    <!--Modernizr (see code at the end of the page -->
    <script src="/Content/USQ/Responsive/Scripts/Vendor/modernizr-2.8.2/modernizr-2.8.2.min.js"></script>


  </head>

  <body>
	
    <!--Header-->
    <header class="usq-header navbar">
	
      <div class="nav-wrapper">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#usq-navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <button type="button" class="navbar-toggle navbar-search" data-toggle="collapse" data-target="#mobile-search">
              <span class="sr-only">Toggle search</span>
              <span class="glyphicon glyphicon-search"></span>
            </button>
            <span class="mobile-logo visible-xs"></span>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <nav class="collapse navbar-collapse" id="usq-navbar">
            <ul class="nav navbar-nav primary-nav">
              <li><a href="http://www.usq.edu.au/study">Study</a></li>
              <li><a href="http://www.usq.edu.au/current-students">Current Students</a></li>
              <li><a href="http://www.usq.edu.au/about-usq">About USQ</a></li>
              <li><a href="http://www.usq.edu.au/research">Research</a></li>
              <li><a href="http://www.usq.edu.au/alumni">Alumni</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right secondary-nav">
              <li><a href="http://www.usq.edu.au/library">Library</a></li>
              <li><a href="http://www.usq.edu.au/jobs">Jobs</a></li>
              <li><a href="http://staffsearch.usq.edu.au/">Staff search</a></li>
              <li><a href="http://www.usq.edu.au/contact">Contact us</a></li>
            </ul>

            <div class="buttons">
              <a class="btn btn--uconnect" href="http://uconnect.usq.edu.au">UConnect</a>
              <a class="btn btn-default btn--askusq btn--light-grey pull-left" href="http://usqstudy.custhelp.com/app/answers/list">
                <span class="btn-icon"></span>
                <div class="btn-label">
                  Ask USQ <br /> Future Students
                </div>
              </a>
              <a class="btn btn-default btn--askusq btn--light-grey pull-right" href="http://usqassist.custhelp.com/app/answers/list/">
                <span class="btn-icon"></span>
                <div class="btn-label">
                  Ask USQ <br /> Current Students
                </div>
              </a>
            </div>

          </nav>
        </div>
      </div>
    </header>

    <!--Body-->
    <div class="main-wrap" id="main-wrap">
	
      <section class="usq-branding hidden-xs">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <div class="logo">
                <a href="#">
                  <img src="/Content/USQ/Responsive/Images/usq-logo.png" alt="usq logo" width="220" height="85" />
                </a>
              </div>
              <div class="brand-search">
                <div class="btn-group">
                  <a class="btn btn-default btn--askusq btn--light-grey dropdown-toggle" data-toggle="dropdown">
                    <span class="btn-icon"></span>
                    <span class="btn-label">Need help? <strong>Ask USQ!</strong></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a class="btn btn--charcoal btn--sm" href="http://usqstudy.custhelp.com/app/answers/list/kw/" onclick="_gaq.push(['_trackEvent', 'future students ask usq', 'internal link', 'header']);">Future Students</a></li>
                    <li><a class="btn btn--charcoal btn--sm" href="http://usqassist.custhelp.com/app/answers/list/kw/" onclick="_gaq.push(['_trackEvent', 'current students ask usq', 'internal link', 'header']);">Current Students</a></li>
                    <li><a class="btn btn--charcoal btn--sm" href="http://usqstudy.custhelp.com/app/chat/chat_launch/session" onclick="_gaq.push(['_trackEvent', 'chat', 'internal link', 'header']);">Chat to us</a></li>
                  </ul>
                </div>
                <form name="search" method="get" action="http://websearch.usq.edu.au/search">
                  <fieldset>
                    <div class="btn-input-field">
                      <input type="search" name="q" value="" class="form-control" placeholder="Search text" />
                      <button class="btn" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
                    <input type="hidden" name="client" value="default_frontend" />
                    <input type="hidden" name="proxystylesheet" value="default_frontend" />
                    <input type="hidden" name="site" value="default_collection" />
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="usq-breadcrumbs">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <ul>
                <li>
                  <a href="http://www.usq.edu.au/">Home</a>
                </li>
                <li>
                  <a href="#">Hazard information poster generator</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>

      <section>
        <div class="container">
          <div class="row">
		  
            <div class="col-sm-3 col-menu">
              <div class="usq-left-menu">
                <a class="left-menu-toggle visible-xs" href="#" data-toggle="collapse" data-target="#usq-left-menu">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </a>
                <div class="menu-heading hidden-xs">
                  Human Resources
                </div>
                <div id="usq-left-menu" class="panel-collapse collapse in">
                  <div class="panel-body">
                    <ul>
                      <li>
                        <a href="/hr/healthsafe/safepers/usqsafetm">
                          <span class="text-label">USQSafe Team</span>
                          <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                      </li>
                      <li>
                        <a href="/hr/healthsafe/safepers/hsrs">
                          <span class="text-label">Health and Safety Representatives (HSRs)</span>
                          <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                      </li>
                      <li>
                        <a href="/hr/healthsafe/safepers/buildwd">
                          <span class="text-label">Chief Wardens</span>
                          <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                      </li>
                      <li class="active">
                        <a href="/hr/healthsafe/safepers/cprfirstaid">
                          <span class="text-label">Employees trained in CPR and First Aid</span>
                          <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                      </li>
                      <li>
                        <a href="/hr/healthsafe/safepers/committees">
                          <span class="text-label">Safety committees</span>
                          <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                      </li>
                      <li>
                        <a href="/hr/healthsafe/safepers/usc">
                          <span class="text-label">University Safety Committee members</span>
                          <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div> <!-- usq-left-menu -->
            </div> <!-- col-sm-3 col-menu -->

            <div class="col-sm-9 col-content">
              <h1>Hazard information poster generator</h1>

              <p>This form allows you to generate Hazard Information posters for laboratories, workshops, plant rooms and other hazardous areas.<br/>
                When you click  the "Submit" button,  it will output an image which you may print (in A3-size, portrait aspect, in colour) then affix to the entrance to the facility.</p>

              <form role="form" method="post" action="hazardscript.php" target="_blank">

                <section class="container-fluid">
                  <h2 class="row">Area details</h2>

                  <div class="row">

                    <div class="form-group col-sm-6">
                      <!--<label for="facility">Facility Name</label>-->
                      <input type="text" id="facility" name="facility" size="40" class="form-control" placeholder="Facility Name" />
                    </div>

                    <div class="form-group col-sm-6">
                      <!--<label for="room">Room Number</label>-->
                      <input type="text" id="room" name="room" size="20" class="form-control" placeholder="Room Number" />
                    </div>

                    <div class="form-group col-sm-6">
                      <!--<label for="faculty">Faculty/Unit</label>-->
                      <input type="text" id="faculty" name="faculty" size="44" class="form-control" placeholder="Faculty/Unit" />
                    </div>

                    <div class="form-group col-sm-6">
                      <!--<label for="room_class">Room Classification</label>-->
                      <select id="room_class" name="room_class" class="form-control">
                        <option value="none">Choose a room classification...</option>
                        <option value="wok">Workshop</option>
                        <option value="lab">Research Lab / Prep Room</option>
                        <option value="ulab">Undergrad Lab</option>
                        <option value="sim">Simulated Activities Room</option>
                        <option value="mfg">Manufacturing Facility</option>
                        <option value="svc">Service Area</option>
                        <option value="plt">Plant Room</option>
                        <option value="off">Office</option>
                      </select>
                    </div>

                  </div>

                </section>

                <section class="container-fluid">
                  <h2 class="row">Safety hazards that may be encountered in this area</h2>

                  <div class="row">
                    <h3 class="col-sm-12">Common Hazards</h3>

                    <div class="col-sm-12">
                      <?php
                      getStickers( "hazard", " col-sm-6 checkbox-warning " );
                      ?>
                    </div>

                  </div>

                  <div class="row">
                    <h3 class="col-sm-12">Custom Hazards (optional)</h3>
					
                    <div class="col-sm-12">
					
                      <div class="form-inline">
                        <!--<label for="cust_haz_1">Custom Hazard #1</label>-->
						<img src="img/stickers/hazard/exclamation.png" class="img-thumbnail thumbnail" /> 
						<input class="form-control" size="50" type="text" name="customHzds[]" id="cust_haz_1" placeholder="Custom Hazard #1" />

                      </div>

                      <div class="form-inline">
                        <!--<label for="cust_haz_2">Custom Hazard #2</label>-->
						<img src="img/stickers/hazard/exclamation.png" class="img-thumbnail thumbnail" /> 
						<input class="form-control" size="50" type="text" name="customHzds[]" id="cust_haz_2" placeholder="Custom Hazard #2" />

                      </div>

                      <div class="form-inline">
                        <!--<label for="cust_haz_3">Custom Hazard #3</label>-->
						<img src="img/stickers/hazard/exclamation.png" class="img-thumbnail thumbnail" /> 
						<input class="form-control" size="50" type="text" name="customHzds[]" id="cust_haz_3" placeholder="Custom Hazard #3" />

                      </div>
                    </div>

                  </div>

                </section>

                <section class="container-fluid">
                  <h2 class="row">Precautionary measures that may be required</h2>

                  <div class="row">
                    <h3 class="col-sm-12">Induction Requirements</h3>

                    <div class="col-sm-12">

                      <div class="radio radio-primary">
						<input type="radio" name="induction" id="ind_hard" value="hard" checked class="" />
                        <label for="ind_hard" >Safety induction required before entering</label>
                      </div>

                      <div class="radio radio-primary">
						<input type="radio" name="induction" id="ind_soft" value="soft" class="" />
                        <label for="ind_soft">Safety induction required before working unsupervised</label>
                      </div>
                      
                      <div class="radio radio-primary">
					    <input type="radio" name="induction" id="ind_ess" value="essentials" class="" />
                        <label for="ind_ess">Safety and Wellbeing Essentials required before working.</label>
                      </div>

                      <div class="input-group usq-bare radio radio-primary">
						<input type="radio" name="induction" id="ind_cust" value="custom" aria-label="Custom induction message" class="" />
						<label for="ind_cust">
						<input type="text" class="form-control" name="customInduct" id="customInduct" size="50" placeholder="Custom induction message (optional)" />
						</label>
                        
                      </div>

                    </div>
                  </div>

                  <div class="row">
                    <h3 class="col-sm-12">Protections</h3>

                    <div>
                      <?php
                      getStickers( "protection", " col-sm-6 checkbox-primary" );
                      ?>
                    </div>

                  </div>

                </section>

                <section class="container-fluid">
                  <h2 class="row">In case of an emergency or accident</h2>

                  <div class="row">
                    <h3 class="col-sm-12">Actions Required</h3>

                    <div>
                      <?php
                      getStickers( "equipment", " col-sm-4 checkbox-success" );
                      ?>
                    </div>

                  </div>

                </section>

                <section class="container-fluid">
                  <h2 class="row">Facility Personnel</h2>

                  <div class="row">
                    <h3 class="col-sm-12">Nominated First Aid Officer </h3>

                    <div class="form-group col-sm-4">
                      <!--<label for="aid1_name">Name</label>-->
                      <input class="form-control" type="text" id="aid1_name" name="aid1_name" placeholder="Name" />
                      <span class="help-block"><a href="https://www.usq.edu.au/hr/healthsafe/safepers/cprfirstaid" target="_blank">View Facility Personnel list</a></span>
                    </div>

                    <div class="form-group col-sm-4">
                      <!--<label for="aid1_room">Room</label>-->
                      <input class="form-control" type="text" id="aid1_room" name="aid1_room" size="15" placeholder="Room" />
                    </div>

                    <div class="form-group col-sm-4">
                      <!--<label for="aid1_ext">Ext</label>-->
                      <input class="form-control" type="text" id="aid1_ext" name="aid1_ext" size="5" placeholder="Ext" />
                    </div>
                  </div>


                  <div class="row">
                    <h3 class="col-sm-12">Responsible Officer</h3>

                    <div class="form-group col-sm-4">
                      <!--<label for="super_name">Name</label>-->
                      <input class="form-control" type="text" id="super_name" name="super_name" placeholder="Name" />
                      <span class="help-block"><a href="https://www.usq.edu.au/hr/healthsafe/safepers/cprfirstaid" target="_blank">View Facility Personnel list</a></span>
                    </div>

                    <div class="form-group col-sm-4">
                      <!--<label for="super_room">Room</label>-->
                      <input class="form-control" type="text" id="super_room" name="super_room" size="15" placeholder="Room" />
                    </div>

                    <div class="form-group col-sm-4">
                      <!--<label for="super_ext">Ext</label>-->
                      <input class="form-control" type="text" id="super_ext" name="super_ext" size="5" placeholder="Ext" />
                    </div>
                  </div>

                  <div class="row">
                    <h3 class="col-sm-12">Technical Support Officer</h3>

                    <div class="form-group col-sm-4">
                      <!--<label for="bso_name">Name</label>-->
                      <input class="form-control" type="text" id="bso_name" name="bso_name" placeholder="Name" />
                      <span class="help-block"><a href="https://www.usq.edu.au/hr/healthsafe/safepers/cprfirstaid" target="_blank">View Facility Personnel list</a></span>
                    </div>

                    <div class="form-group col-sm-4">
                      <!--<label for="bso_room">Room</label>-->
                      <input class="form-control" type="text" id="bso_room" name="bso_room" size="15" placeholder="Room" />
                    </div>

                    <div class="form-group col-sm-4">
                      <!--<label for="bso_ext">Ext</label>-->
                      <input class="form-control" type="text" id="bso_ext" name="bso_ext" size="5" placeholder="Ext" />
                    </div>

                  </div>

                </section>

                <section>

                  <h3 class="col-sm-12">Hazard Inspections</h3>

                  <div class="form-group col-sm-6">
                    <label for="last_inspect">Last Inspection conducted on </label>
                    <input class="form-control" type="date" id="last_inspect" name="last_inspect" />
                    <span class="help-block">Date Format: dd/MM/YYYY</span>
                  </div>

                  <div class="form-group col-sm-6">
                    <label for="next_inspect">Next Inspection due on </label>
                    <input class="form-control" type="date" id="next_inspect" name="next_inspect" />
                    <span class="help-block">Date Format: dd/MM/YYYY</span>
                  </div>

                </section>

                <section>

                  <h3 class="col-sm-12">Poster File Generation</h3>

                  <div class="form-group col-sm-8">

                    <label>Output file type:
                      <select class="form-control" name="filetype">
                        <option value="none" selected>none</option>
                        <option value="gif">.gif</option>
                        <option value="jpeg">.jpeg</option>
                        <option value="pdf">.pdf</option>
                        <option value="png">.png</option>
                      </select>
                    </label>
                  </div>

                  <div class="col-sm-4">
                    <input class="btn btn-lg btn--charcoal" type="submit" value="Generate poster" name="submit" />
                  </div>

                </section>

              </form>

            </div> <!-- col-sm-9 col-content -->
          </div><!--row-->
        </div><!--container-->

      </section>

    </div> <!-- main-wrap -->

    <!--Footer-->
    <footer>
      <div class="section-one bg-yellow">
        <div class="container">
          <div class="row">
            <div class="link-wrap">
              <div class="col-sm-2 link-box">
                <a data-toggle="collapse" href="#about-links">
                  About
                </a>
                <hr class="visible-xs" />
                <div id="about-links" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul>
                      <li><a href="http://www.usq.edu.au/alumni/giving-to-usq">Giving to USQ</a></li>
                      <li><a href="http://www.usq.edu.au/jobs">Careers at USQ</a></li>
                      <li><a href="http://www.usq.edu.au/news-events">News and Events</a></li>
                      <li><a href="http://www.usq.edu.au/about-usq/campuses-maps">Campuses and Maps</a></li>
                      <li><a href="http://www.usq.edu.au/about-usq/faculties-sections">Faculties and Sections</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-sm-2 link-box">
                <a data-toggle="collapse" href="#footer-study">
                  Study
                </a>
                <hr class="visible-xs" />
                <div id="footer-study" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul>
                      <li><a href="http://www.usq.edu.au/study/degrees">Degrees and Courses</a> </li>
                      <li><a href="http://www.usq.edu.au/study/degrees/pathway-programs">Pathways to Study</a></li>
                      <li><a href="http://www.usq.edu.au/study/apply/credit">Credit for Previous Study</a> </li>
                      <li><a href="http://www.usq.edu.au/study/apply">How to Apply</a> </li>
                      <li><a href="http://library.usq.edu.au/">Library</a> </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-sm-2 link-box">
                <a data-toggle="collapse" href="#footer-contact">
                  Contact
                </a>
                <hr class="visible-xs" />
                <div id="footer-contact" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul>
                      <li><a href="http://www.usq.edu.au/contact">Contact Us</a></li>
                      <li><a href="http://www.usq.edu.au/az">A-Z Directory</a></li>
                      <li><a href="http://staffsearch.usq.edu.au">Staff Directory</a></li>
                      <li><a href="http://www.usq.edu.au/news-events/contact">Media Enquiries</a></li>
                      <li><a href="http://usqstudy.custhelp.com/ci/documents/detail/2/regform">Join the USQ family</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-sm-2 link-box">
                <a data-toggle="collapse" href="#footer-research">
                  Research
                </a>
                <hr class="visible-xs" />
                <div id="footer-research" class="panel-collapse collapse">
                  <div class="panel-body">
                    <ul>
                      <li><a href="http://www.usq.edu.au/research/research-at-usq/institutes-centres">Research Institutes and Centres</a></li>
                      <li><a href="http://www.usq.edu.au/research/research-students">Research Students</a></li>
                      <li><a href="http://www.usq.edu.au/research/support-development">Research Support and Development</a></li>
                      <li><a href="http://webapp.usq.edu.au/expertfinder/?kw=">Find a Researcher</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-3 col-sm-push-1">

              <div class="ask-usq-buttons">
                <a class="btn btn--charcoal btn--w100" href="http://usqstudy.custhelp.com/" onclick="ga('send', 'event', 'ask usq', 'internal link', 'footer');">
                  <span class="glyphicon glyphicon-chevron-right pull-right"></span>
                  <span>Ask USQ</span>
                </a>
                <a class="btn btn--light-grey btn--w100 visible-xs" href="tel:1800269500">
                  <span class="glyphicon glyphicon-earphone pull-right"></span>
                  <span>Call us</span>
                </a>
                <a class="btn btn--light-grey btn--w100 hidden-xs" href="http://www.usq.edu.au/contact">
                  <span class="glyphicon glyphicon-earphone pull-right"></span>
                  <span>Call us</span>
                </a>
                <a class="btn btn--light-grey btn--w100" href="mailto:study@usq.edu.au">
                  <span class="glyphicon glyphicon-envelope pull-right"></span>
                  <span>Email us</span>
                </a>
              </div>

            </div>
          </div>
          <div class="row acknowledgement">
            <div class="col-sm-2 logo">
              <a href="/"><span></span></a>
            </div>
            <div class="col-sm-5 first-peoples">
              <a href="http://www.usq.edu.au/about-usq/acknowledgement-of-first-peoples">Acknowledgement of First Peoples</a>
            </div>
          </div>

        </div>
      </div>
      <!--Black section-->
      <div class="section-two bg-charcoal">
        <div class="container">
          <div class="row">
            <div class="col-sm-3 social">
              <ul>
                <li><a target="_blank" class="tw" href="https://twitter.com/usqedu">Twitter</a></li>
                <li><a target="_blank" class="fb" href="https://www.facebook.com/usqedu">Facebook</a></li>
                <li><a target="_blank" class="yt" href="http://www.youtube.com/user/usqedu">Youtube</a></li>
                <li><a target="_blank" class="li" href="http://www.linkedin.com/company/university-of-southern-queensland">LinkedIn</a></li>
                <li><a target="_blank" class="wp" href="http://usqedu.wordpress.com/">Wordpress</a></li>
                <li><a class="ig" href="http://instagram.com/usqedu">Instagram</a></li>
                <li><a target="_blank" class="pi" href="http://pinterest.com/usqedu">Pinterest</a></li>
                <li><a target="_blank" class="gp" href="https://plus.google.com/+UsqEduAu/posts">Google plus</a></li>
              </ul>
            </div>
            <div class="col-sm-9 links">
              <div class="legal">
                <span>ABN: 40 234 732 081</span>
                <span>CRICOS: QLD 00244B, NSW 02225M</span>
                <span>TEQSA: PRV12081</span>
                <span><a href="http://www.usq.edu.au/records/rtiandlegaldiscovery">Right to information</a></span>
                <span><a href="http://www.usq.edu.au/about-site/disclaimer">Disclaimer</a></span>
                <span><a href="http://www.usq.edu.au/about-site/privacy">Privacy</a></span>
                <span><a href="http://www.usq.edu.au/about-site/feedback">Feedback</a></span>
                <span><a href="http://www.usq.edu.au/contact">Contact us</a></span>
              </div>
              <div class="copyright">
                <div class="network"><a target="_blank" href="http://run.edu.au/">USQ is a Member of the Regional Universities Network</a></div>
                <div>&copy; University of Southern Queensland 2014</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>

    <!-- Scripts -->
    <script>
      Modernizr.load([
        {
          load: '//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js',
          complete: function () {
            if (!window.jQuery) {
              Modernizr.load('/Content/USQ/Responsive/Scripts/Vendor/jquery-2.1.0/jquery-2.1.0.min.js');
            }
          }
        },
        "/Content/USQ/Responsive/Scripts/Vendor/bootstrap-3.2/carousel.js",
        "/Content/USQ/Responsive/Scripts/Vendor/bootstrap-3.2/collapse.js",
        "/Content/USQ/Responsive/Scripts/Vendor/bootstrap-3.2/dropdown.js",
        "/Content/USQ/Responsive/Scripts/Vendor/bootstrap-3.2/transition.js",
        "/Content/USQ/Responsive/Scripts/Vendor/bootstrap-3.2/tab.js",
        "/Content/USQ/Responsive/Scripts/Vendor/fancybox-2.1.5/jquery.fancybox.js",
        "/Content/USQ/Responsive/Scripts/Vendor/fancybox-2.1.5/jquery.fancybox-media.js",
        "/Content/USQ/Responsive/Scripts/Vendor/matchMedia/matchMedia.js",
        "/Content/USQ/Responsive/Scripts/Vendor/touchswipe-1.6/touchswipe.js",
        "/Content/USQ/Responsive/Scripts/Vendor/enquire-2.1.0/enquire-2.1.0.min.js",
        "/Content/USQ/Responsive/Scripts/Vendor/jquery.placeholder-2.0.7/jquery.placeholder.min.js",
        "/Content/USQ/Responsive/Scripts/Vendor/responsive-tabs/responsive-tabs.js",
        "/Content/USQ/Responsive/Scripts/Vendor/jquery.cookie-1.4.0/jquery.cookie.js",
        "/Content/USQ/Responsive/Scripts/image-resizer.js",
        "/Content/USQ/Responsive/Scripts/announcement-handler.js",
        "/Content/USQ/Responsive/Scripts/banner-handler.js",
        "/Content/USQ/Responsive/Scripts/fancybox-handler.js",
        "/Content/USQ/Responsive/Scripts/mobile-menu-handler.js",
        "/Content/USQ/Responsive/Scripts/programdetail-tab-handler.js",
        "/Content/USQ/Responsive/Scripts/dropdown-content-filter.js",
        "/Content/USQ/Responsive/Scripts/important-dates.js",
        "/Content/USQ/Responsive/Scripts/link-tables.js",
        "/Content/USQ/Responsive/Scripts/master.js",
        // Hazard Poster Generator scripts
        "js/tabber.js",
        "js/randomimage.js",
        "js/utils.js"
      ]);

    </script>

  </body>
</html>
